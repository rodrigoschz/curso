const assert = require('assert');
const Web3 = require('web3');

const provider = new Web3.providers.HttpProvider("HTTP://127.0.0.1:7545");
const web3 = new Web3(provider);

const [interface , bytecode] = require('../scripts/compile');

let accounts; 
let userContracts;

beforeEach(async() => {
    accounts = await web3.eth.getAccounts();
    userContracts = await new web3.eth.Contract(JSON.parse(interface))
        deploy({data: bytecode})
        .send({from: accounts[0], gas: '1000000'});
});

describe('The UsersContract', async() => {
    it('should deploy',() => { 
        console.log(userContracts.options.address);
        assert.ok(userContracts.options.address);
    });
});